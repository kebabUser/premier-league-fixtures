package com.example.premierleaguefixtures

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.premierleaguefixtures.databinding.FragmentFootballMatchBinding

class FootballMatchAdapter: RecyclerView.Adapter<FootballMatchAdapter.FootballMatchHolder> {
    class FootballMatchHolder(item :View):RecyclerView.ViewHolder(item){
        val binding = FragmentFootballMatchBinding.bind(item)
        fun bind(footballMatch: FootballMatch) = with(binding){
            homeTeam.text = footballMatch.firstTeam
            homeTeamScore.inputType = footballMatch.pointsOfFirstTeam
            awayTeamScore.inputType = footballMatch.pointsOfSecondTeam
            awayTeam.text = footballMatch.secondTeam

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FootballMatchHolder {
        TODO("Not yet implemented")
    }

    override fun onBindViewHolder(holder: FootballMatchHolder, position: Int) {
        TODO("Not yet implemented")
    }

    override fun getItemCount(): Int {
        TODO("Not yet implemented")
    }
}