package com.example.premierleaguefixtures

data class FootballMatch(val firstTeam : String, val secondTeam : String, val pointsOfFirstTeam : Int, val pointsOfSecondTeam : Int)